#!/usr/bin/env python
# Sample Qt image viewer based on pyqtgraph using
# GraphicsView and ImageItem
# Original version by Tiago Pereira 09-Mar-2020
# Added possibility of giving filename, gamma factor
# (displays image**gamma).
# Translated histo_opt to Python and include cutoff
# (for histo_opt(image,cutoff=cutoff)) to set scaling
# of images by V. Hansteen 15-Apr-2020
# Now using time.time() instead of pyqtgraph.ptime() Mod. 20230327 ASD

from PyQt5.QtCore import Qt, QTimer
from pyqtgraph.Qt import QtGui
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QLabel, QSizePolicy, QSlider, QSpacerItem, \
    QVBoxLayout, QWidget, QToolButton, QStyle, QFileDialog
import pyqtgraph as pg
import time 
import numpy as np
import getopt, sys
from astropy.io import fits
fileName=''

def histo_opt(image, cutoff=1.5e-3, gamma=1.0, verbose=True):
    """
    Computes and returns the min and max values of the input (image), clipping brightest and darkest pixels.

    levels = histo_opt(image, cutoff=1.5e3, gamma=1.0, verbose=True)
    Based on original IDL routine by P.Suetterlin (06-Jul-1993)
    Ported (more or less the same algorithm) to Python by V.Hansteen 15-Apr-2020
    """
    import numpy as np
    hmin = np.min(image)
    hmax = np.max(image)
    if isinstance(image[0][0],np.float32):
      nbins = 10000
      fak = np.float(nbins)/(hmax-hmin)
      hist = np.histogram((image-hmin)*fak,range=(0.,np.float(nbins)),bins=nbins)
    else:
      nbins = np.max(image)-np.min(image)
      hist = np.histogram(image,bins=nbins)
      fak = 1.
    h = hist[0]
    bin = hist[1]
    nh = np.size(h)    
    #
    # Integrate the histogram so that h(i) holds the number of points
    # with equal or lower intensity.
    #
    for i in range(1,nh-1):
       h[i] = (h[i]+h[i-1])
 
    h = h/np.float(h[nh-2])
    h[nh-1] = 1.
    #
    # As cutoff is in percent and h is normalized to unity,
    # vmin/vmax are the indices of the point where the number of pixels
    # with lower/higher intensity reach the given limit. This has to be
    # converted to a real image value by dividing by the scalefactor
    # fak and adding the min value of the image
    # Note that the bottom value is taken off (addition of h[0] to cutoff),
    # there are often very many points in IRIS images that are set to zero, this
    # removes them from calculation... and seems to work.
    #
    vmin = (np.max(np.where(h <= (cutoff+h[0]),bin[1:]-bin[0],0))/fak+hmin)**gamma
    vmax = (np.min(np.where(h >= (1.-cutoff),bin[1:]-bin[0],nh-2))/fak+hmin)**gamma
    if verbose:
      print('{0:.2e}/{1:.2e} image min/image max'.format(np.min(image)**gamma,np.max(image)**gamma))
      print('{0:.2e}/{1:.2e} histo_opt min/histo_opt max'.format(vmin,vmax))
    
    return (vmin,vmax)

class Slider(QWidget):
    def __init__(self, minimum, maximum, parent=None):
        super(Slider, self).__init__(parent=parent)
        self.horizontalLayout = QHBoxLayout(self)
        self.label = QLabel(self)

        self.verticalLayout = QVBoxLayout()
        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.Horizontal)
        self.verticalLayout.addWidget(self.slider)

        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.addWidget(self.label)
        self.resize(self.sizeHint())

        self.slider.setMinimum(minimum)
        self.slider.setMaximum(maximum)

        self.slider.valueChanged.connect(self.setLabelValue)
        self.setLabelValue(self.slider.value())

    def setLabelValue(self, value):
        self.label.setText("%i" % value)


class Widget(QWidget):
    def __init__(self, parent=None, fileName='', gamma=1.0, cutoff=1.e-3, verbose=False):
        super(Widget, self).__init__(parent=parent)
        
        # Load data, file dialog works, but not used at present (would have to change
        # call in __main__
        if fileName=='':
          file = QFileDialog.getOpenFileName(self,"Open Image", ".","Image Files (*.fits)")
          fileName=file[0]
          self.fileName=fileName
        if verbose:
          print('Viewing {}'.format(fileName))
          print('image**( Gamma = {:.3f})'.format(gamma))
          print('histo_opt(image, Cutoff = {:.3f} )'.format(cutoff))
        self.gamma=gamma
        self.cutoff=cutoff
        self.verbose=verbose

        fobs = fits.open(fileName,memmap=True, do_not_scale_image_data=True)
        self.data = np.transpose(fobs[0].data, axes=(0, 2, 1))
        # translated histo_opt from IDL, seems that it is OK so far
        self.levels = histo_opt(np.array(self.data[0],dtype='uint16'),
                                    gamma=gamma,cutoff=cutoff,verbose=verbose)

        # With GraphicsView and ImageItem
        win = pg.GraphicsView(useOpenGL=True)
        view = pg.ViewBox()
        win.setCentralItem(view)
        ## lock the aspect ratio so pixels are always square
        view.setAspectLocked(True)
        #view.invertY()
        self.image = pg.ImageItem()
        # Original levels (dtype not set) were levels - (-32200, -29000)
        self.image.setImage(np.array(self.data[0],dtype="uint16")**gamma, levels=self.levels)
        view.addItem(self.image)

        # Add image
        self.verticalLayout = QVBoxLayout(self)
        #self.verticalLayout.addWidget(self.image)  # for ImageView
        self.verticalLayout.addWidget(win)  # for ImageItem

        # Add slider
        self.snap_slider = Slider(0, self.data.shape[0] - 1)
        self.verticalLayout.addWidget(self.snap_slider)

        # Add button
        self.play_button = QToolButton(clicked=self.update_button)  # was update_timer
        self.play_button.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.verticalLayout.addWidget(self.play_button)

        # Add timer
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_timer)

        self.a = 0
        self.playing = False

        # For updating image in static case
        self.snap_slider.slider.valueChanged.connect(self.update)

        self.updateTime = time.time()
        self.fps = 0

    def update_button(self):
        if self.playing:
            self.timer.stop()
            self.playing = False
            self.play_button.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
            self.play_button.repaint()
        else:
            self.timer.start(1)
            self.playing = True
            self.play_button.setIcon(self.style().standardIcon(QStyle.SP_MediaPause))
            self.play_button.repaint()

    def update_timer(self):
        i = self.a % self.data.shape[0]
        self.a += 1
        self.snap_slider.slider.setValue(i)


    def update(self):
        self.a = self.snap_slider.slider.value()
        self.image.setImage(np.array(self.data[self.a],dtype="uint16")**self.gamma,
                                levels=self.levels,
                                autoHistogramRange=False, autoRange=False)

    def set_gamma(self,gamma=1.0):
        self.gamma = gamma

    def set_fileName(self, fileName=''):
        self.fileName = fileName

    def set_cutoff(self, cutoff=1.e-3):
        self.cutoff = cutoff
        
    def set_verbose(self, verbose=False):
        self.cutoff = cutoff

def sji_files(dir='.', filter='*SJI*.fits'):
    from pathlib import Path
    p = Path(dir)
    i = 0
    name = []
    for file in p.glob(filter):
        print('[{0}] {1}'.format(i,file))
        i += 1
        name.append(file)
    return name
        
def sji_view(fileName, gamma=1, cutoff=1.e-3, verbose=False):
    """View IRIS SJI movie using GraphicsView."""
    import os
    
    if not os.path.isfile(fileName):
        print ("*** Error: {} does not exist, exiting".format(fileName))
        sys.exit(1)

    app = QApplication(sys.argv)

    w = Widget(fileName=fileName, gamma=gamma, cutoff=cutoff, verbose=verbose)
    w.resize(800,800)
    w.setWindowTitle('{0} Gamma = {1:.3f}'.format(fileName,gamma))
    w.show()
    sys.exit(app.exec_())
    
if __name__ == '__main__':
    import os
    
    full_cmd_arguments = sys.argv
    argument_list = full_cmd_arguments[1:]
    short_options = "hg:c:v"
    long_options = ["help", "gamma=", "cutoff=", "verbose"]
    try:
      arguments, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
      print (str(err))
      sys.exit(2)

    if values == []: values.append('')
    fileName=values[0]
    gamma = 1.0
    cutoff = 1.5e-3
    verbose = False
      
    for current_argument, current_value in arguments:
      if current_argument in ("-v", "--verbose"):
        print ("Enabling verbose mode")
        verbose = True
      elif current_argument in ("-h", "--help"):
        print ("Usage: {}  -h -v -g -c <fitsinputfile>".format(sys.argv[0]))
      elif current_argument in ("-g", "--gamma"):
        gamma = np.float(current_value)
      elif current_argument in ("-c", "--cutoff"):
        cutoff = np.float(current_value)

    if fileName == '':
        print ("*** Error: Filename must be given: {}  -h -v -g -c <fitsinputfile>".format(sys.argv[0]))
        sys.exit(1)
    if not os.path.isfile(fileName):
        print ("*** Error: {} does not exist, exiting".format(fileName))
        sys.exit(1)

    sji_view(fileName, gamma=gamma, cutoff=cutoff, verbose=verbose)

    

    
