# LMSAL_HUB/IRIS_HUB

LMSAL_HUB/IRIS_HUB is a python library focused on interfacing with code and
projects for IRIS.

Currently, the library is a loose collection of different scripts and classes
with varying degrees of portability and usefulness.

## Installation (only python routines)

To make use of LMSAL_HUB/IRIS_HUB, before attempting to install LMSAL_HUB/IRIS_HUB you need the following:

* [Python](http://www.python.org) (2.7.x, 3.4.x or later)
* [Astropy](http://astropy.org)
* [NumPy](http://numpy.scipy.org/)
* [SciPy](http://www.scipy.org/)
* [Scikit-learn](https://scikit-learn.org/stable/)
* [Joblib](https://joblib.readthedocs.io/en/latest/)

The following packages are also recommended to take advantage of all the features:

* [Matplotlib](http://matplotlib.sourceforge.net/) (1.1+)
* [h5py](http://www.h5py.org/)
* [netCDF4](https://unidata.github.io/netcdf4-python/)
* [Cython](http://www.cython.org)
* [pandas](http://pandas.pydata.org/)
* [beautifulsoup4](http://www.crummy.com/software/BeautifulSoup/)

IRIS_HUB will install without the above packages, but functionality will be limited.

All of the above Python packages are available through [Anaconda](https://docs.continuum.io/anaconda/), and that is the recommended way of setting up your Python distribution.

Next, use git to grab the latest version of LMSAL_HUB/IRIS_HUB:

     mkdir -p LMSAL_HUB/iris_hub
     cd LMSAL_HUB/iris_hub
     git clone https://gitlab.com/LMSAL_HUB/iris_hub/iris_lmsalpy.git
     cd iris_lmsalpy
     python setup.py install

### Non-root install

If you don't have write permission to your Python packages directory, use the following option with `setup.py`:

     python setup.py install --user

This will install LMSAL_HUB/iris_hub under your home directory (typically `~/.local`).

### Developer install

If you want to install LMSAL_HUB/iris_hub but also actively change the code or contribute to its development, it is recommended that you do a developer install instead:

     python setup.py develop

This will set up the package such as the source files used are from the git repository that you cloned (only a link to it is placed on the Python packages directory). Can also be combined with the `--user` flag for local installs.


## Documentation

Documentation is available in the IRIS tutorial:

 * [*"A quick-start guide to work with IRIS Level2 data in Python"* (HTML)](https://iris.lmsal.com/itn45/index.html)
 * [*"A quick-start guide to work with IRIS Level2 data in Python"* (PDF)](https://iris.lmsal.com/itn45/itn45.pdf)

or at:

 - http://LMSAL_HUB/iris_hub.readthedocs.org
